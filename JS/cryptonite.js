$(function () {
  /* 
    //  from here it is for live repoart page it is to drow the chart
    //  It must be located at the top of the page
    */
  $("#chartLink").on("click", function () {
    $("#progressBar").show(0);
    $("#progressBar").hide(2500);

    clearInterval(IntervalId);
    createselectedCoinsSymbol();
    $("#LiveReports").empty();
    if (selectedCoinsSymbol.length == 0) {
      $("#LiveReports").html(`<div class="container-fluid">
                                        <div class="row">
                                            <div class="col-10">
                                           <h1>You did not select any coin,
                                           Please select between 1 to 5 coins for display graph!</h1>
                                            </div>
                                        </div>
                                 </div>`);
    } else {
      $(".progressBar").show();
      let arrCoinRealTime1 = [];
      let arrCoinRealTime2 = [];
      let arrCoinRealTime3 = [];
      let arrCoinRealTime4 = [];
      let arrCoinRealTime5 = [];
      let arrCoinRealTimeName = [];
      function getData() {
        $.ajax({
          type: "GET",
          url: `https://min-api.cryptocompare.com/data/pricemulti?fsyms=${selectedCoinsSymbol}&tsyms=USD`,
          beforeSend: function () {},
          success: function (result) {
            if (result.Response == "Error") {
              clearInterval(IntervalId);
              $(".progressBar").hide();
              $("#LiveReports").html(
                `<div class="noneselectedmsg"> <h2>No information to display for in selected coins Please try other coins!</h2> </div>`
              );
            } else {
              $("#LiveReports").html(
                ` <div id="chartContainer" class="startHiden" style="height: 300px; width: 100%;"></div>`
              );
              let dateNow = new Date();
              let counter = 1;
              arrCoinRealTimeName = [];

              for (let key in result) {
                if (counter == 1) {
                  arrCoinRealTime1.push({ x: dateNow, y: result[key].USD });
                  arrCoinRealTimeName.push(key);
                }
                if (counter == 2) {
                  arrCoinRealTime2.push({ x: dateNow, y: result[key].USD });
                  arrCoinRealTimeName.push(key);
                }
                if (counter == 3) {
                  arrCoinRealTime3.push({ x: dateNow, y: result[key].USD });
                  arrCoinRealTimeName.push(key);
                }
                if (counter == 4) {
                  arrCoinRealTime4.push({ x: dateNow, y: result[key].USD });
                  arrCoinRealTimeName.push(key);
                }
                if (counter == 5) {
                  arrCoinRealTime5.push({ x: dateNow, y: result[key].USD });
                  arrCoinRealTimeName.push(key);
                }
                counter++;
              }
              createGraph();
            }
          },
        });
      }

      IntervalId = setInterval(() => {
        getData();
      }, 2000);

      function createGraph() {
        let chart = new CanvasJS.Chart("chartContainer", {
          exportEnabled: true,
          animationEnabled: false,
          title: {
            text: "The price of the coins in real time * is updated every 2 seconds",
          },
          axisX: {
            title: "Time",
            valueFormatString: "HH:mm:ss",
          },
          axisY: {
            title: "Coins Value",
            suffix: "$",
            titleFontColor: "#4F81BC",
            lineColor: "#4F81BC",
            labelFontColor: "#4F81BC",
            tickColor: "#4F81BC",
            includeZero: true,
          },
          toolTip: {
            shared: true,
          },
          legend: {
            cursor: "pointer",
            itemclick: coinsFromData,
          },
          data: [
            {
              type: "spline",
              name: arrCoinRealTimeName[0],
              showInLegend: true,
              xValueFormatString: "HH:mm:ss",
              dataPoints: arrCoinRealTime1,
            },
            {
              type: "spline",
              name: arrCoinRealTimeName[1],
              showInLegend: true,
              xValueFormatString: "HH:mm:ss",
              dataPoints: arrCoinRealTime2,
            },
            {
              type: "spline",
              name: arrCoinRealTimeName[2],
              showInLegend: true,
              xValueFormatString: "HH:mm:ss",
              dataPoints: arrCoinRealTime3,
            },
            {
              type: "spline",
              name: arrCoinRealTimeName[3],
              showInLegend: true,
              xValueFormatString: "HH:mm:ss",
              dataPoints: arrCoinRealTime4,
            },
            {
              type: "spline",
              name: arrCoinRealTimeName[4],
              showInLegend: true,
              xValueFormatString: "HH:mm:ss",
              dataPoints: arrCoinRealTime5,
            },
          ],
        });
        chart.render();
        function coinsFromData(e) {
          if (
            typeof e.dataSeries.visible === "undefined" ||
            e.dataSeries.visible
          ) {
            e.dataSeries.visible = false;
          } else {
            e.dataSeries.visible = true;
          }
          e.chart.render();
        }
      }
    }
  });

  /* 
   //   here it is end of live repoart page 
   //  and from here it is home and about page
   */
  let allCoinsUrl = "https://api.coingecko.com/api/v3/coins";
  let moreInfoUrl = "https://api.coingecko.com/api/v3/coins/";
  let moreInfoId = "";
  let allCoins = [];
  let selectedCoins = [];
  let singelCoin = {};
  let str = "";
  let moreInfoStr = "";
  let popupStr = "";
  let add6CoinToSelectedCoin = "";
  let coinForRemove = "";
  let getCoinFromSearch = null;
  let selectedCoinsSymbol = [];
  let IntervalId;
  let selectedCoinsForDrow = [];

  /* this function is for call to server to get info from the api */
  let callToServer = (url, type) => {
    $.ajax({
      type: "GET",
      datatype: "json",
      url: url,
      async: false,
      beforeSend: function () {
        $("#progressBar").show(0);
      },
      success: function (resulte) {
        if (type == 1) {
          allCoins = resulte;
          drowAllCoins(allCoins);
        } else if (type == 2) {
          singelCoin = resulte;
        }
        $("#progressBar").hide(0);
      },
      error: function (error) {
        console.log("error : ", error);
      },
    });
  };

  /* this hide is for hiden all page on lode */
  $(".startHiden").hide(0);
  $("#myModal").modal("hide");

  /* this function is for show selected page and hid all the ather from the html */
  $(".nav-link").on("click", function () {
    $("a").removeClass("Theactive");
    $(this).addClass("Theactive");
    let theSelectNav = $(this).attr("myId");
    $(".discription").hide(0);
    $("#" + theSelectNav).show(350);
    if (theSelectNav != "Home") {
      $(".onlyHome").hide(0);
    } else {
      $(".onlyHome").show(0);
      drowAllCoins(allCoins);
      infoBtn();
      editSelectedCoin();
      markCheckedOnlyCoinsInSelectedCoinsArr();
    }
  });

  /* for show and hide live reporat */
  $(".hideRep").on("click", function () {
    $("#LiveReports").removeClass("d-md-block");
  });
  $("#chartLink").on("click", function () {
    $("#LiveReports").addClass("d-md-block");
  });

  /* this function is for get all selected coins from local storag */
  let getSelectedCoinsFromLoacalStorage = () => {
    let selectedCoinsFromLocalStorage = localStorage.getItem("selectedCoins");
    if (selectedCoinsFromLocalStorage != null) {
      selectedCoins = JSON.parse(selectedCoinsFromLocalStorage);
    }
  };

  /*this function is for mark checked or unchecked in all coins card,  it is mark checked only for coin fron seelcted coin array*/
  let markCheckedOnlyCoinsInSelectedCoinsArr = () => {
    $(".cb").prop("checked", false);
    if (selectedCoins != null) {
      selectedCoins.map((coin) => {
        $("#r_" + coin).prop("checked", true);
      });
    }
  };

  /* this function is for get the coin symbol from the selected coins array  */
  let createselectedCoinsSymbol = () => {
    for (i = 0; i < selectedCoins.length; i++) {
      allCoins.map((coin) => {
        if (coin.id == selectedCoins[i]) {
          selectedCoinsSymbol += `${coin.symbol},`;
        }
      });
    }
  };

  /*this function is for drow all card insert the html*/
  let drowAllCoins = async (coinList) => {
    $("#progressBar").show(0);
    getSelectedCoinsFromLoacalStorage();
    str = "";
    coinList.map((coin, i) => {
      drowSingelCoin(coin, i);
    });
    document.getElementById("Home").innerHTML = str;
    markCheckedOnlyCoinsInSelectedCoinsArr();
    drowSelectedCoins();
    $("#progressBar").hide(0);
  };

  /* this function is for drow singel card With coin ditales */
  let drowSingelCoin = (coin, i) => {
    str += `<div class="col-md-4 mt-2">`;
    str += `<div class="card">`;
    str += `<div class="custom-control custom-switch">`;
    str += `<input type="checkbox" myId='${coin.id}' class="custom-control-input cb" id="r_${coin.id}">`;
    str += `<label class="custom-control-label checkBox mt-2 float-right" for="r_${coin.id}"></label>`;
    str += `</div>`;
    str += `<div class="card-body">`;
    str += `<h5 class="card-title">${coin.symbol}</h5>`;
    str += `<p class="card-text">${coin.name}</p>`;
    str += `<button data-toggle="collapse" myId="moreInfoText${i}" href="#moreInfoText${i}" aria-expanded="true" aria-controls="moreInfoText"  id=${coin.id} type="button" class="infoBtn btn btn-info mb-2">More Info</button>`;
    str += `<div id="moreInfoText${i}" class="card-text"></div>`;
    str += `</div>`;
    str += `</div>`;
    str += `</div>`;
  };

  let drowSelectedCoins = () => {
    createselectedCoinsSymbol();
    let selectStr = ``;
    selectStr = `You have selected these coins: ${selectedCoinsSymbol} `;
    document.getElementById("showSelecteCoins").innerHTML = selectStr;
    selectedCoinsSymbol = "";
  };

  /* here i call to the sarver and drow all coins in cards */
  callToServer(allCoinsUrl, 1);

  /* this function is for get now time it is for use limted tume life in local storage */
  let now = () => {
    let time = performance.now();
    return time;
  };

  /* this function is for save more info coin to loacl storage */
  let setLocalStorage = (coin, val) => {
    localStorage.setItem(
      coin,
      JSON.stringify({
        ttl: 120000,
        now: now(),
        val: val,
      })
    );
  };

  /* this function is for get more info coin from loacl storage */
  let getLocalStorage = (coin) => {
    var input = JSON.parse(localStorage.getItem(coin));
    if (!input) {
      return null;
    }
    if (input.ttl && input.ttl + input.now < now()) {
      localStorage.removeItem(coin);
      return null;
    }
    return input.val;
  };
  /* this function is for more info button */
  let infoBtn = () => {
    $(".infoBtn").on("click", function () {
      moreInfoId = $(this).attr("id");
      moreInfoUrl += moreInfoId;
      if ($("#" + moreInfoId).text() == "Close") {
        $("#" + moreInfoId).text("More Info");
      } else {
        $("#" + moreInfoId).text("Close");
        let moreInfoTextId = $(this).attr("myId");
        if (getLocalStorage(moreInfoId) == null) {
          callToServer(moreInfoUrl, 2);
          bildMoreInfo();
          document.getElementById(`${moreInfoTextId}`).innerHTML = moreInfoStr;
          setLocalStorage(moreInfoId, moreInfoStr);
        }
        if (getLocalStorage(moreInfoId) != null) {
          moreInfoStr = getLocalStorage(moreInfoId);
          document.getElementById(`${moreInfoTextId}`).innerHTML = moreInfoStr;
        }
      }
      moreInfoUrl = "https://api.coingecko.com/api/v3/coins/";
      moreInfoId = "";
    });
  };
  infoBtn();
  /* this function is for bild more info text */
  let bildMoreInfo = () => {
    moreInfoStr = "";
    singelCoin.image.small
      ? (moreInfoStr += `<img class="my-2" src="${singelCoin.image.small}" alt="non image">`)
      : (moreInfoStr += `<p>`);
    singelCoin.market_data.current_price.usd
      ? (moreInfoStr += `<p>${singelCoin.market_data.current_price.usd}$ </p>`)
      : (moreInfoStr += `<p> No value available in USD</p>`);
    singelCoin.market_data.current_price.eur
      ? (moreInfoStr += `<p>${singelCoin.market_data.current_price.eur}\u20ac </p>`)
      : (moreInfoStr += `<p> No value available in EUR</p>`);
    singelCoin.market_data.current_price.ils
      ? (moreInfoStr += `<p>${singelCoin.market_data.current_price.ils}&#8362 </p>`)
      : (moreInfoStr += `<p> No value available in ILS</p>`);
  };

  /* this function is for Add or remove coin from selected coins arr  */
  let editSelectedCoin = () => {
    $(".cb").change(function (e) {
      let checked = $(this).is(":checked");
      if (selectedCoins.length < 5) {
        if (checked == true) {
          let selectCoin = $(this).attr("myId");
          selectedCoins.push(selectCoin);
          console.log(selectedCoins);
          localStorage.setItem("selectedCoins", JSON.stringify(selectedCoins));
          drowSelectedCoins();
        } else {
          selectedCoins = selectedCoins.filter(
            (value) => value !== $(this).attr("myId")
          );
          localStorage.setItem("selectedCoins", JSON.stringify(selectedCoins));
          drowSelectedCoins();
          console.log(selectedCoins);
        }
      } else {
        if (checked == true) {
          drowPopUp();
          this.checked = false;
          add6CoinToSelectedCoin = $(this).attr("myId");
          popupStr = "";
          $("#myModal").modal("show");
        } else {
          selectedCoins = selectedCoins.filter(
            (value) => value !== $(this).attr("myId")
          );
          drowSelectedCoins();
          localStorage.setItem("selectedCoins", JSON.stringify(selectedCoins));
        }
      }
    });
  };
  editSelectedCoin();

  /* this function is for drow the popup window to limited  only 5 coins for reports */
  let drowPopUp = () => {
    selectedCoins.map((coin, i) => {
      popupStr += `<div class="form-check custom-control custom-switch ml-4">`;
      popupStr += `<input name="check" onclick="onlyOne(this)" coinName='${coin}'  type="checkbox" id='selectedCoins${i}'  class="custom-control-input selectCb" data-toggle="toggle">`;
      popupStr += `<label class="custom-control-label checkBox" for='selectedCoins${i}'>`;
      popupStr += `${coin}`;
      popupStr += `</label >`;
      popupStr += `</div >`;
      document.getElementById("modalText").innerHTML = popupStr;
    });
    coinNameForRemove();
  };

  /* this function is for Get the name of the coin for the deletion of selected coins ARR */
  let coinNameForRemove = () => {
    $(".selectCb").on("click", function () {
      let checked = $(this).is(":checked");
      if (checked == true) {
        coinForRemove = $(this).attr("coinName");
      }
    });
  };

  /* this function is for remove the coin is selected from popup  */
  $("#popupSave").on("click", function () {
    let index = selectedCoins.findIndex((coin) => {
      return coinForRemove == coin;
    });
    if (index != -1) {
      selectedCoins.splice(index, 1);
      localStorage.setItem("selectedCoins", JSON.stringify(selectedCoins));
      drowSelectedCoins();
    }
    selectedCoins.push(add6CoinToSelectedCoin);
    localStorage.setItem("selectedCoins", JSON.stringify(selectedCoins));
    drowSelectedCoins();
    $("#myModal").modal("hide");
    markCheckedOnlyCoinsInSelectedCoinsArr();
  });
  console.log(selectedCoins);

  $("#showBtn").on("click", function () {
    let theCoin;
    selectedCoins.map((sCoin) => {
      theCoin = sCoin;
      if (selectedCoins != []) {
        allCoins.map((coin) => {
          if (coin.id == theCoin) {
            selectedCoinsForDrow.push(coin);
          }
        });
      }
    });
    if (selectedCoinsForDrow.length > 0) {
      drowAllCoins(selectedCoinsForDrow);
      infoBtn();
      editSelectedCoin();
      markCheckedOnlyCoinsInSelectedCoinsArr();
    } else {
      alert("No coins to display. Please select coin and try again");
      drowAllCoins(allCoins);
      infoBtn();
      editSelectedCoin();
      markCheckedOnlyCoinsInSelectedCoinsArr();
    }
    selectedCoinsForDrow = [];
  });
  $("#allCoinsBtn").on("click", function () {
    drowAllCoins(allCoins);
    infoBtn();
    editSelectedCoin();
    markCheckedOnlyCoinsInSelectedCoinsArr();
  });

  /* this function is for search coin in client and get the value from the input  || this function it is onclick function  */
  $("#btnSearch").on("click", function () {
    let valueSearchText = document.getElementById("inputSearch").value;
    getCoinFromSearch = [];
    if (valueSearchText != null) {
      allCoins.map((coin) => {
        if (coin.symbol == valueSearchText) {
          getCoinFromSearch.push(coin);
        }
      });
      if (getCoinFromSearch.length > 0) {
        drowAllCoins(getCoinFromSearch);
        infoBtn();
        editSelectedCoin();
        markCheckedOnlyCoinsInSelectedCoinsArr();
      } else {
        alert(
          "No results for the requested expression. You must search by full symbol name"
        );
        drowAllCoins(allCoins);
        infoBtn();
        editSelectedCoin();
        markCheckedOnlyCoinsInSelectedCoinsArr();
      }
    }
    getCoinFromSearch = null;
    document.getElementById("inputSearch").value = "";
  });

  /* here it is the end off on load */
});

/* this function is for Allow only 1 check box in the popup to be on checked  */
let onlyOne = (checkbox) => {
  var checkboxes = document.getElementsByName("check");
  checkboxes.forEach((item) => {
    if (item !== checkbox) item.checked = false;
  });
};
